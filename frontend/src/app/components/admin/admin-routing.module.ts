import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditdataComponent } from './editdata/editdata.component';
import { EditsubjectComponent } from './editsubject/editsubject.component';
import { EdituserComponent } from './edituser/edituser.component';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {path: 'editdata', component: EditdataComponent},
  {path: 'editsubject', component: EditsubjectComponent},
  {path: 'edituser', component: EdituserComponent}
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
