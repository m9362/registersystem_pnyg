import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourseComponent } from './course/course.component';
import { RouterModule, Routes } from '@angular/router';
// import { countReset } from 'console';

const routes: Routes = [
  {path: 'course', component: CourseComponent}
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class TeacherRoutingModule { }
