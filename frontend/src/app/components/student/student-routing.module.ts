import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {AddSubjectComponent} from './add-subject/add-subject.component'
import {ChangeSubjectComponent} from './change-subject/change-subject.component'
import {DeleteSubjectComponent} from './delete-subject/delete-subject.component'
import {RegisterSubjectComponent} from './register-subject/register-subject.component'
import { RecordComponent } from './record/record.component';
import { ResultSubjectComponent } from './result-subject/result-subject.component';
import { ScheduleComponent } from './schedule/schedule.component';
const routes: Routes = [
  {path: 'addsubject', component: AddSubjectComponent},
  {path: 'changesubject', component: ChangeSubjectComponent},
  {path: 'delsubject', component: DeleteSubjectComponent},
  {path: 'regissubject', component: RegisterSubjectComponent},
  {path: 'record', component: RecordComponent},
  {path: 'resultsubject', component: ResultSubjectComponent},
  {path: 'schedule', component: ScheduleComponent},
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class StudentRoutingModule { }
