import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder } from '@angular/forms';
import { RecordSub } from './record';
@Component({
  selector: 'app-record',
  templateUrl: './record.component.html',
  styleUrls: ['./record.component.css']
})
export class RecordComponent implements OnInit {

  selected = '2/2564';
  recordsubs !: RecordSub[];

  public token: any;
  constructor(private fb: FormBuilder,private _api: ApiService,private auth: AuthService,) {

   }

  ngOnInit(): void {
    console.log(this.selected)
    this.Cal();
  }

  Cal(): void{
    this.token = this.auth.getToken();
    console.log(this.selected)
    const employees: Array<{years: string, token: any}> = [{
      years:this.selected,token:this.token
    }]

    console.log(employees)
    this._api.postTypeRequest('student/record',employees ).subscribe((res: any) => {
        this.recordsubs = res.data;
        console.log(this.recordsubs)
    })
  }



}
