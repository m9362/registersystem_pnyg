import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder } from '@angular/forms';
import { RegisterSub } from './register-subject';

@Component({
  selector: 'app-register-subject',
  templateUrl: './register-subject.component.html',
  styleUrls: ['./register-subject.component.css']
})
export class RegisterSubjectComponent implements OnInit {
  selected = '2/2564'
  registersubs !: RegisterSub[];
  public token: any;
  constructor(private fb: FormBuilder,private _api: ApiService,private auth: AuthService,) { }

  ngOnInit(): void {
    this.Cal();
  }

  Cal(): void{
    this.token = this.auth.getToken();
    console.log(this.selected)
    const employees: Array<{years: string, token: any}> = [{
      years:this.selected,token:this.token
    }]

    console.log(employees)
    this._api.postTypeRequest('student/course',employees ).subscribe((res: any) => {
        console.log(res)
      this.registersubs = res.data;
        console.log(this.registersubs)
    })
  }

  onSave(a: RegisterSub): void{
    this.token = this.auth.getToken();
    if(a.sec){
      console.log(a)
    console.log(this.selected)
    const employees: Array<{courses: string,sec:string, token: any}> = [{
      courses:a.courseID,sec:a.sec,token:this.token
    }]

    console.log(employees)
    this._api.postTypeRequest('student/register',employees ).subscribe((res: any) => {
        console.log(res)
    })
    }else{
      alert("โปรดใส่ตอนเรียนด้วย")
    }

  }

}
