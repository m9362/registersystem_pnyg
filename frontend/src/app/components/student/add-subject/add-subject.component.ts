import { Component, OnInit } from '@angular/core';
import { AddSub } from './add-subject';
import { ApiService } from 'src/app/services/api.service';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder, FormGroup,Validators } from '@angular/forms';

@Component({
  selector: 'app-add-subject',
  templateUrl: './add-subject.component.html',
  styleUrls: ['./add-subject.component.css']
})
export class AddSubjectComponent implements OnInit {

  addForm !: FormGroup;
  addsubs !: AddSub[];

  constructor(private fb: FormBuilder,private _api: ApiService,private auth: AuthService,) {
    this.addForm = this.fb.group({
      courseid: ['', Validators.required],
      sec: ['', Validators.required],
    })
  }

  ngOnInit(): void {
  }


  onSubmit(a:AddSub): void{
    a.token = this.auth.getToken()
    console.log(a)
    this._api.postTypeRequest('student/add', a).subscribe((res: any) => {
      if(res.status == 2 ){
        alert("เพิ่มวิชาสำเร็จ")
      }else if(res.status == 1 ){
        alert("รหัสวิชาผิด")
      }else{
        alert("เกิดข้อผิดพลาด ตรวจสอบอีกครั้ง")
      }
      console.log(res)
    });


    this.addForm.reset()
  }

}
