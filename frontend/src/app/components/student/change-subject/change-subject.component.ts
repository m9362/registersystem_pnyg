import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder, FormGroup,Validators } from '@angular/forms';
import { ChangeSub } from './change-subject';

@Component({
  selector: 'app-change-subject',
  templateUrl: './change-subject.component.html',
  styleUrls: ['./change-subject.component.css']
})
export class ChangeSubjectComponent implements OnInit {

  changeForm !: FormGroup;
  changesubs !: ChangeSub[];

  constructor(private fb: FormBuilder,private _api: ApiService,private auth: AuthService) {
    this.changeForm = this.fb.group({
      courseid: ['', Validators.required],
      news: ['', Validators.required],
      olds: ['', Validators.required],
    })
   }

  ngOnInit(): void {
  }

  onSubmit(a:ChangeSub): void{
    a.token = this.auth.getToken()
    console.log(a)
    this._api.postTypeRequest('student/change',a).subscribe((res: any) => {
      if(res.status == 2 ){
        alert("เปลี่ยนวิชาสำเร็จ")
      }else if(res.status == 0 ){
        alert("รหัสวิชา หรือ ตอนเรียนผิดพลาด")
      }else{
        alert("เกิดข้อผิดพลาด ตรวจสอบอีกครั้ง")
      }
    });

    this.changeForm.reset()
  }

}
