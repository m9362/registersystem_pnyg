import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder } from '@angular/forms';
import { DeleteSub } from './delete-subject';

@Component({
  selector: 'app-delete-subject',
  templateUrl: './delete-subject.component.html',
  styleUrls: ['./delete-subject.component.css']
})
export class DeleteSubjectComponent implements OnInit {
  selected = '2/2564';
  deletesubs !: DeleteSub[];
  public token: any;
  constructor(private fb: FormBuilder,private _api: ApiService,private auth: AuthService,) { }

  ngOnInit(): void {
    this.Cal();
  }

  Cal(): void{
    this.token = this.auth.getToken();
    console.log(this.selected)
    const employees: Array<{years: string, token: any}> = [{
      years:this.selected,token:this.token
    }]

    console.log(employees)
    this._api.postTypeRequest('student/record',employees ).subscribe((res: any) => {
        this.deletesubs = res.data;
        console.log(this.deletesubs)
    })
  }

  onSave(course: any): void{
    this.token = this.auth.getToken();
    console.log(this.selected)
    const employees: Array<{courses: string, token: any}> = [{
      courses:course,token:this.token
    }]

    console.log(employees)
    // this._api.postTypeRequest('student/delete',employees ).subscribe((res: any) => {
    //     console.log(res)
    // })
  }

}
