import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentComponent } from 'src/app/components/student/student.component';
import { TeacherComponent } from 'src/app/components/teacher/teacher.component';
import { AdminComponent } from 'src/app/components/admin/admin.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: 'students', component: StudentComponent},
  {path: 'teachers', component: TeacherComponent},
  {path: 'admins', component: AdminComponent},
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class LoginRoutingModule { }
