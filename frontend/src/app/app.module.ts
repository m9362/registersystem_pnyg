import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth/auth.module';
import { ProfileComponent } from './main/profile/profile.component';
import { InterceptorService } from './services/interceptor-service.service';
import { StudentComponent } from './components/student/student.component';
import { StudentRoutingModule } from './components/student/student-routing.module';
import { RegisterSubjectComponent } from './components/student/register-subject/register-subject.component';
import { AddSubjectComponent } from './components/student/add-subject/add-subject.component';
import { DeleteSubjectComponent } from './components/student/delete-subject/delete-subject.component';
import { ChangeSubjectComponent } from './components/student/change-subject/change-subject.component';
import { ResultSubjectComponent } from './components/student/result-subject/result-subject.component';
import { RecordComponent } from './components/student/record/record.component';
import { ScheduleComponent } from './components/student/schedule/schedule.component';
import { TeacherComponent } from './components/teacher/teacher.component';
import { TeacherRoutingModule } from './components/teacher/teacher-routing.module';
import { CourseComponent } from './components/teacher/course/course.component';
import { AdminComponent } from './components/admin/admin.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginRoutingModule } from './auth/components/login/login-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    StudentComponent,
    RegisterSubjectComponent,
    AddSubjectComponent,
    DeleteSubjectComponent,
    ChangeSubjectComponent,
    ResultSubjectComponent,
    RecordComponent,
    ScheduleComponent,
    TeacherComponent,
    CourseComponent,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    AuthModule,
    StudentRoutingModule,
    TeacherRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    LoginRoutingModule

  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
