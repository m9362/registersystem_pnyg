const express = require('express');
const router = express.Router();
const md5 = require('md5');
const jwt = require('jsonwebtoken');

const mysql = require('mysql');

const con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "registersystem1"
});


/* GET users listing. */
router.post('/register', async function (req, res, next) {
  try {
    let { username, email, password } = req.body; 
   
    const hashed_password = md5(password.toString())

    const checkUsername = `Select username FROM user WHERE username = ?`;
    con.query(checkUsername, [username], (err, result, fields) => {
      if(!result.length){
        const sql = `Insert Into user (username, password) VALUES ( ?,  ? )`
        con.query(
          sql, [username, hashed_password],
        (err, result, fields) =>{
          if(err){
            res.send({ status: 0, data: err });
          }else{
            let token = jwt.sign({ data: result }, 'secret')
            res.send({ status: 1, data: result, token : token });
          }
         
        })
      }
    });
  } catch (error) {
    res.send({ status: 0, error: error });
  }
});

router.post('/login', async function (req, res, next) {
  try {
    let { username, password } = req.body; 
    // const hashed_password = md5(password.toString())
    const sql = `SELECT * FROM user WHERE username = ? AND password = ?`
   
    con.query(
      sql, [username, password],
    function(err, result, fields){
      console.log(result.length)
      if(result.length){
      console.log("1")
      if(err){
        console.log("2")
        res.send({ status: 0});
      }else{
        console.log(result[0].level)
        let token = jwt.sign({  userid: result[0].userID }, 'secret123')
        console.log(token)
        res.send({ status: 2, level:result[0].level, token: token });
      }
    }else{
      res.send({ status: 1});
    }
    console.log("3")
    })
  } catch (error) {
    res.send({ status: 0, error: error });
  }
});



module.exports = router;