const express = require('express');
const router = express.Router();
const md5 = require('md5');
const jwt = require('jsonwebtoken');

const mysql = require('mysql');

const con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "registersystem1"
});

router.post('/add', async function (req, res, next) {
  console.log(req.body)
  try {
    let { courseid, sec, token } = req.body; 
    console.log(courseid)
    const checkCourse = `Select year FROM course WHERE courseID = ?`;
    con.query(checkCourse, [courseid], (err, result, fields) => {
      console.log(result)
      if(result.length){
        console.log("year"+result[0].year)
        let years = result[0].year;
        console.log(token)
        const decoded = jwt.verify(token, 'secret123')
        console.log(decoded.userid)
        // console.log(result)
        const sqlstudent = `SELECT studentID FROM student WHERE userID= ?`;
          con.query(sqlstudent, [decoded.userid], (err, result, fields) => {
            console.log("studentid "+result[0].studentID)
            const sql = `Insert Into register (courseID, studentID,year,section) VALUES ( ?,  ?, ? , ? )`
            con.query(
            sql, [courseid, result[0].studentID,years,sec],
            (err, result, fields) =>{
            if(err){
              res.send({ status: 0, data: err });
            }else{
              res.send({ status: 2 });
          }
         
        })
          })
      }
      else{
        res.send({ status: 1 })
      }
      
    });
  } catch (error) {
    res.send({ status: 0, error: error });
  }
});

router.post('/change', async function (req, res, next) {
  console.log(req.body)
  try {
    let { courseid, news,olds, token } = req.body; 
    const decoded = jwt.verify(token, 'secret123')
    console.log(decoded.userid)
    const sqlstudent = `SELECT studentID FROM student WHERE userID= ?`;
          con.query(sqlstudent, [decoded.userid], (err, result, fields) => {
            if(result.length){
            console.log("studentid "+result[0].studentID)
            const sql = `UPDATE register SET section = ? WHERE courseID= ? AND studentID= ?`
            con.query(
            sql, [news,courseid, result[0].studentID],
            (err, result, fields) =>{
            if(err){
              res.send({ status: 0, data: err });
            }else{
              res.send({ status: 2 });
          }
         
          })
        }
        else{
          res.send({ status: 1 });
        }
          })
    
  } catch (error) {
    res.send({ status: 0, error: error });
  }
});

router.post('/record', async function (req, res, next) {
  console.log(req.body)
  try {
    let { years, token } = req.body[0]; 
    console.log('years'+years)
    const decoded = jwt.verify(token, 'secret123')
    console.log(decoded.userid)
    const sqlstudent = `SELECT studentID FROM student WHERE userID= ?`;
          con.query(sqlstudent, [decoded.userid], (err, result, fields) => {
            if(result.length){
            console.log("studentid "+result[0].studentID)
            const sql = `SELECT register.courseID AS courseid, register.section AS sec,course.courseName AS name , course.credit AS cred
            FROM register
            INNER JOIN course ON course.courseID=register.courseID WHERE register.studentID = ? AND register.year = ?;`
            con.query(
            sql, [result[0].studentID,years],
            (err, result, fields) =>{
              // console.log("result: "+result[0].courseid)
            if(err){
              res.send({ status: 0, data: err });
            }else{
              res.send({ status: 2,data:result });
          }
         
          })
        }
        else{
          res.send({ status: 1 });
        }
          })
    
  } catch (error) {
    res.send({ status: 0, error: error });
  }
});

router.post('/delete', async function (req, res, next) {
  console.log(req.body)
  try {
    let { courses, token } = req.body[0]; 
    console.log('years'+courses)
    const decoded = jwt.verify(token, 'secret123')
    console.log(decoded.userid)
    const sqlstudent = `SELECT studentID FROM student WHERE userID= ?`;
          con.query(sqlstudent, [decoded.userid], (err, result, fields) => {
            if(result.length){
            console.log("studentid "+result[0].studentID)
            const sql = `DELETE FROM register WHERE courseID = ? AND studentID = ?;`
            con.query(
            sql, [courses,result[0].studentID],
            (err, result, fields) =>{
              // console.log("result: "+result[0].courseid)
            if(err){
              res.send({ status: 0, data: err });
            }else{
              res.send({ status: 2});
          }
         
          })
        }
        else{
          res.send({ status: 1 });
        }
          })
    
  } catch (error) {
    res.send({ status: 0, error: error });
  }
});

router.post('/course', async function (req, res, next) {
  // console.log(req.body)
  try {
    console.log(req.body[0])
    let { years,token } = req.body[0]; 
    console.log('years'+years)
    const sqlstudent = `SELECT courseID,courseName FROM course WHERE year = ?`;
          con.query(sqlstudent, [years], (err, result, fields) => {
            if(result.length){
              console.log(result)
              res.send({ status: 2,data:result });
        }
        else{
          res.send({ status: 1 });
        }
          })
    
  } catch (error) {
    res.send({ status: 0, error: error });
  }
});

router.post('/regiseter', async function (req, res, next) {
  console.log(req.body)
  try {
    let { courses,sec, token } = req.body[0]; 
    // console.log('years'+courses)
    const decoded = jwt.verify(token, 'secret123')
    console.log(decoded.userid)
    const sqlstudent = `SELECT studentID FROM student WHERE userID= ?`;
          con.query(sqlstudent, [decoded.userid], (err, result, fields) => {
            if(result.length){
            console.log("studentid "+result[0].studentID)
            const sql = `;`
            con.query(
            sql, [courses,result[0].studentID],
            (err, result, fields) =>{
              // console.log("result: "+result[0].courseid)
            if(err){
              res.send({ status: 0, data: err });
            }else{
              res.send({ status: 2});
          }
         
          })
        }
        else{
          res.send({ status: 1 });
        }
          })
    
  } catch (error) {
    res.send({ status: 0, error: error });
  }
});

router.post('/all', async function (req, res, next) {
  console.log(req.body)
  try {
    let { studentID, year, token } = req.body; 
    console.log(studentID)
    const allCourse = 'SELECT * FROM register WHERE studentID=? && year = ?;';
    con.query(allCourse, [studentID, year], (err, result, fields) => {
      console.log(result)
      if(result.length){
        console.log("year"+result[0].year)
        let years = result[0].year;
        console.log(token)
        const decoded = jwt.verify(token, 'secret123')
        console.log(decoded.userid)
        // console.log(result)
        const sqlstudent = 'SELECT studentID FROM student WHERE userID= ?';
          con.query(sqlstudent, [decoded.userid], (err, result, fields) => {
            console.log("studentid "+result[0].studentID)
            const sql = 'SELECT * FROM register WHERE studentID=? && year = ?';
            con.query(
            sql, [result[0].studentID,years],
            (err, result, fields) =>{
            if(err){
              res.send({ status: 0, data: err });
            }else{
              res.send({ status: 2 });
          }

        })
          })
      }
      else{
        res.send({ status: 1 })
      }

    });
  } catch (error) {
    res.send({ status: 0, error: error });
  }
});

module.exports = router;

