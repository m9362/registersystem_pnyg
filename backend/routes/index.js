const express = require('express');
const router = express.Router();


const user = require('./user');
const profile = require('./profile');
const student = require('./student');


router.use('/user', user);
router.use('/profile', profile);
router.use('/student', student);


module.exports = router;